/*
 * yaepg.c: A plugin for the Video Disk Recorder
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: yaepg.h,v 1.3 2004/11/08 01:41:20 bball Exp $
 */

#ifndef _YAEPG__H
#define _YAEPG__H

#include <vdr/plugin.h>
#include <stdarg.h>
#include "i18n.h"

#define YAEPG_ERROR(...)		cYaepg::LogMsg(YLOG_ERR, __VA_ARGS__)
#define YAEPG_INFO(...)			cYaepg::LogMsg(YLOG_INF, __VA_ARGS__)

typedef enum {
	YLOG_ERR	= 0,
	YLOG_INF	= 1
} eYlog_t;

static const char *VERSION        = "0.0.2";
static const char *DESCRIPTION    = "Yet Another EPG";
static const char *MAINMENUENTRY  = "Yaepg";

#endif /* _YAEPG__H */
